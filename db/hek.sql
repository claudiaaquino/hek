/*
Navicat MySQL Data Transfer

Source Server         : Triade Local
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : hek

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2018-07-12 19:47:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for reservations
-- ----------------------------
DROP TABLE IF EXISTS `reservations`;
CREATE TABLE `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `resource_id` int(11) DEFAULT NULL,
  `resident_room_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reservations
-- ----------------------------
INSERT INTO `reservations` VALUES ('70', '1', '3', '15', '', '2018-07-11 03:00:00', '2018-07-11 04:00:00', '2018-07-09', '2018-07-09 02:32:21');
INSERT INTO `reservations` VALUES ('65', '2', null, '4', '', '2018-07-12 03:00:00', '2018-07-12 04:00:00', '2018-07-08', '2018-07-09 01:36:43');
INSERT INTO `reservations` VALUES ('58', '2', null, '5', '', '2018-07-11 07:00:00', '2018-07-11 08:00:00', '2018-07-08', '2018-07-09 01:22:36');
INSERT INTO `reservations` VALUES ('59', '2', null, '6', '', '2018-07-12 11:00:00', '2018-07-12 12:00:00', '2018-07-08', '2018-07-09 01:22:47');
INSERT INTO `reservations` VALUES ('60', '1', '3', '7', '', '2018-07-14 11:00:00', '2018-07-14 12:00:00', '2018-07-08', '2018-07-09 01:23:05');
INSERT INTO `reservations` VALUES ('61', '1', '4', '8', '', '2018-07-12 10:00:00', '2018-07-12 11:00:00', '2018-07-08', '2018-07-09 01:23:24');
INSERT INTO `reservations` VALUES ('62', '1', '1', '9', '', '2018-07-13 12:00:00', '2018-07-13 13:00:00', '2018-07-08', '2018-07-09 01:23:37');
INSERT INTO `reservations` VALUES ('63', '1', '3', '10', '', '2018-07-11 07:00:00', '2018-07-11 08:00:00', '2018-07-08', '2018-07-09 01:24:27');
INSERT INTO `reservations` VALUES ('66', '2', null, '12', '', '2018-07-12 06:00:00', '2018-07-12 07:00:00', '2018-07-08', '2018-07-09 01:41:35');
INSERT INTO `reservations` VALUES ('67', '1', '1', '8', '', '2018-07-12 19:00:00', '2018-07-12 20:00:00', '2018-07-09', '2018-07-09 13:52:32');
INSERT INTO `reservations` VALUES ('68', '3', null, '12', '', '2018-07-12 09:00:00', '2018-07-12 10:00:00', '2018-07-09', '2018-07-09 02:30:14');
INSERT INTO `reservations` VALUES ('69', '1', '1', '14', 'teste', '2018-07-12 01:00:00', '2018-07-12 02:00:00', '2018-07-09', '2018-07-09 02:31:42');
INSERT INTO `reservations` VALUES ('71', '3', null, '16', '', '2018-07-12 02:00:00', '2018-07-12 03:00:00', '2018-07-09', '2018-07-09 02:33:27');
INSERT INTO `reservations` VALUES ('72', '1', '1', '10', '', '2018-07-14 19:00:00', '2018-07-14 20:00:00', '2018-07-09', '2018-07-09 13:51:57');
INSERT INTO `reservations` VALUES ('74', '1', '1', '17', '', '2018-07-10 20:00:00', '2018-07-10 21:00:00', '2018-07-09', '2018-07-09 13:52:42');
INSERT INTO `reservations` VALUES ('76', '1', '3', '18', 'Hehe', '2018-07-12 07:00:00', '2018-07-12 08:00:00', '2018-07-12', '2018-07-12 18:54:26');
INSERT INTO `reservations` VALUES ('77', '1', '1', '19', 'Nice', '2018-07-13 17:00:00', '2018-07-13 18:00:00', '2018-07-12', '2018-07-12 18:54:36');
INSERT INTO `reservations` VALUES ('78', '1', '1', '20', '', '2018-07-10 15:00:00', '2018-07-10 16:00:00', '2018-07-12', '2018-07-12 18:54:53');
INSERT INTO `reservations` VALUES ('79', '1', '1', '20', 'bla', '2018-07-10 12:00:00', '2018-07-10 13:00:00', '2018-07-12', '2018-07-12 19:03:10');
INSERT INTO `reservations` VALUES ('80', '1', '4', '18', 'MEEEEINS', '2018-07-13 05:00:00', '2018-07-13 06:00:00', '2018-07-12', '2018-07-12 18:56:51');
INSERT INTO `reservations` VALUES ('81', '3', null, '22', '', '2018-07-10 10:00:00', '2018-07-10 11:00:00', '2018-07-12', '2018-07-12 18:59:50');
INSERT INTO `reservations` VALUES ('94', '1', '1', '24', '', '2018-07-10 03:00:00', '2018-07-10 08:00:00', '2018-07-12', '2018-07-12 19:03:08');
INSERT INTO `reservations` VALUES ('84', '21', null, '17', '', '2018-07-11 04:00:00', '2018-07-11 08:00:00', '2018-07-12', '2018-07-12 19:02:26');
INSERT INTO `reservations` VALUES ('85', '21', null, '17', '', '2018-07-11 04:00:00', '2018-07-11 08:00:00', '2018-07-12', '2018-07-12 19:02:26');
INSERT INTO `reservations` VALUES ('86', '21', null, '17', '', '2018-07-11 05:00:00', '2018-07-11 09:00:00', '2018-07-12', '2018-07-12 19:02:26');
INSERT INTO `reservations` VALUES ('87', '21', null, '17', '', '2018-07-11 04:00:00', '2018-07-11 08:00:00', '2018-07-12', '2018-07-12 19:02:26');
INSERT INTO `reservations` VALUES ('88', '21', null, '17', '', '2018-07-11 04:00:00', '2018-07-11 08:00:00', '2018-07-12', '2018-07-12 19:02:26');
INSERT INTO `reservations` VALUES ('89', '21', null, '17', '', '2018-07-11 04:00:00', '2018-07-11 08:00:00', '2018-07-12', '2018-07-12 19:02:26');
INSERT INTO `reservations` VALUES ('90', '21', null, '17', '', '2018-07-11 04:00:00', '2018-07-11 08:00:00', '2018-07-12', '2018-07-12 19:02:26');
INSERT INTO `reservations` VALUES ('91', '21', null, '17', '', '2018-07-11 04:00:00', '2018-07-11 08:00:00', '2018-07-12', '2018-07-12 19:02:26');
INSERT INTO `reservations` VALUES ('92', '21', null, '17', '', '2018-07-11 04:00:00', '2018-07-11 08:00:00', '2018-07-12', '2018-07-12 19:02:26');
INSERT INTO `reservations` VALUES ('93', '21', null, '17', '', '2018-07-11 07:00:00', '2018-07-11 08:00:00', '2018-07-12', '2018-07-12 19:02:34');
INSERT INTO `reservations` VALUES ('95', '1', '2', '6', '', '2018-07-11 04:00:00', '2018-07-11 09:00:00', '2018-07-12', '2018-07-12 19:03:32');
INSERT INTO `reservations` VALUES ('96', '1', '1', '26', '', '2018-07-14 02:00:00', '2018-07-14 07:00:00', '2018-07-12', '2018-07-12 19:11:58');

-- ----------------------------
-- Table structure for resources
-- ----------------------------
DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_schedulable` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1' COMMENT '1',
  `created` varchar(255) DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of resources
-- ----------------------------
INSERT INTO `resources` VALUES ('1', '1', 'Wischi', null, '1', '1', null, '2018-07-08 15:43:53');
INSERT INTO `resources` VALUES ('2', '1', 'Waschi', null, '1', '1', null, '2018-07-08 15:43:53');
INSERT INTO `resources` VALUES ('3', '1', 'Woschi', null, '1', '1', null, '2018-07-08 15:43:53');
INSERT INTO `resources` VALUES ('4', '1', 'Trockner', null, '1', '1', null, '2018-07-08 15:43:54');
INSERT INTO `resources` VALUES ('5', '3', 'Terrasse', null, '1', '1', null, '2018-07-12 18:58:51');
INSERT INTO `resources` VALUES ('6', '3', 'Garten', null, '1', '1', null, '2018-07-12 18:58:52');
INSERT INTO `resources` VALUES ('7', '27', 'teste 1', 'teste 1', '1', '0', '7/12/18, 5:18 PM', '2018-07-12 19:19:12');

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_dormitory` tinyint(1) DEFAULT NULL,
  `is_schedulable` tinyint(1) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  `has_resources` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1' COMMENT '1',
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rooms
-- ----------------------------
INSERT INTO `rooms` VALUES ('1', 'Waschmaschine', 'Waschmaschine', '0', '1', null, '1', '1', null, '2018-07-05 18:27:17');
INSERT INTO `rooms` VALUES ('2', 'Kino & Musik Raum', 'Kino  & Musik', '0', '1', null, '0', '1', null, '2018-07-12 19:15:36');
INSERT INTO `rooms` VALUES ('3', 'Grillen', 'Grillen', '0', '1', null, '1', '1', null, '2018-07-12 19:00:07');
INSERT INTO `rooms` VALUES ('4', '616', '616', '1', '0', null, '0', '1', null, null);
INSERT INTO `rooms` VALUES ('5', '512', null, '1', null, null, null, '1', '2018-07-08 11:22:36', '2018-07-09 01:22:36');
INSERT INTO `rooms` VALUES ('6', '121', null, '1', null, null, null, '1', '2018-07-08 11:22:47', '2018-07-09 01:22:47');
INSERT INTO `rooms` VALUES ('7', '544', null, '1', null, null, null, '1', '2018-07-08 11:23:05', '2018-07-09 01:23:05');
INSERT INTO `rooms` VALUES ('8', '111', null, '1', null, null, null, '1', '2018-07-08 11:23:24', '2018-07-09 01:23:24');
INSERT INTO `rooms` VALUES ('9', '115', null, '1', null, null, null, '1', '2018-07-08 11:23:37', '2018-07-09 01:23:37');
INSERT INTO `rooms` VALUES ('10', '114', null, '1', null, null, null, '1', '2018-07-08 11:24:27', '2018-07-09 01:24:27');
INSERT INTO `rooms` VALUES ('11', '151', null, '1', null, null, null, '1', '2018-07-08 11:25:20', '2018-07-09 01:25:20');
INSERT INTO `rooms` VALUES ('12', '112', null, '1', null, null, null, '1', '2018-07-08 11:41:35', '2018-07-09 01:41:35');
INSERT INTO `rooms` VALUES ('13', '016', null, '1', null, null, null, '1', '2018-07-09 12:26:32', '2018-07-09 02:26:32');
INSERT INTO `rooms` VALUES ('14', '223', null, '1', null, null, null, '1', '2018-07-09 12:31:02', '2018-07-09 02:31:02');
INSERT INTO `rooms` VALUES ('15', '441', null, '1', null, null, null, '1', '2018-07-09 12:32:21', '2018-07-09 02:32:21');
INSERT INTO `rooms` VALUES ('16', '666', null, '1', null, null, null, '1', '2018-07-09 12:33:27', '2018-07-09 02:33:27');
INSERT INTO `rooms` VALUES ('17', '222', null, '1', null, null, null, '1', '2018-07-09 11:52:42', '2018-07-09 13:52:42');
INSERT INTO `rooms` VALUES ('18', '420', null, '1', null, null, null, '1', '2018-07-12 04:54:26', '2018-07-12 18:54:26');
INSERT INTO `rooms` VALUES ('19', '213', null, '1', null, null, null, '1', '2018-07-12 04:54:36', '2018-07-12 18:54:36');
INSERT INTO `rooms` VALUES ('20', '204', null, '1', null, null, null, '1', '2018-07-12 04:54:53', '2018-07-12 18:54:53');
INSERT INTO `rooms` VALUES ('21', 'Allgemeines', null, null, '1', null, null, '1', null, null);
INSERT INTO `rooms` VALUES ('22', '233', null, '1', null, null, null, '1', '2018-07-12 04:59:50', '2018-07-12 18:59:50');
INSERT INTO `rooms` VALUES ('24', '333', null, '1', null, null, null, '1', '2018-07-12 05:03:08', '2018-07-12 19:03:08');
INSERT INTO `rooms` VALUES ('25', '123', null, '1', null, null, null, '1', '2018-07-12 05:06:32', '2018-07-12 19:06:32');
INSERT INTO `rooms` VALUES ('26', '520', null, '1', null, null, null, '1', '2018-07-12 05:11:58', '2018-07-12 19:11:58');
INSERT INTO `rooms` VALUES ('27', 'teste', 'teste', '0', '1', null, '1', '0', '2018-07-12 17:17:42', '2018-07-12 19:19:48');
