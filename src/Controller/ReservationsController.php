<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Reservations Controller
 *
 * @property \App\Model\Table\ReservationsTable $Reservations
 *
 * @method \App\Model\Entity\Reservation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReservationsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Rooms', 'Resources', 'Residents']
        ];
        $reservations = $this->paginate($this->Reservations);

        $this->set(compact('reservations'));
    }

    /**
     * search method
     *
     * @return \Cake\Http\Response|void
     */
    public function search() {
        $where = [
            'room_id' => $this->request->getData('room_id'),
            'start_time > (start_time - INTERVAL 7 DAY)',
            'start_time < (start_time + INTERVAL 2 MONTH)'
        ];

        if ($resource_id = $this->request->getData('resource_id')) {
            $where['resource_id'] = $resource_id;
        }
        $reservations = $this->Reservations->find('all')->contain('Residents')->where($where);
//        print_r($reservations->first());
        $this->set(compact('reservations'));
        $this->set('_serialize', ['reservations']);
    }

    /**
     * View method
     *
     * @param string|null $id Reservation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $reservation = $this->Reservations->get($id, [
            'contain' => ['Rooms', 'Resources', 'Residents']
        ]);

        $this->set('reservation', $reservation);
    }

    /**
     * add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $data = (array) $this->request->input('json_decode');
        $resident_room = $this->Reservations->Rooms->find('all')->where([
                    'name' => $data['resident_room'],
                    'is_dormitory' => true])->first();

        if (!$resident_room) {
            $resident_room = $this->Reservations->Rooms->newEntity();
            $resident_room->name = $data['resident_room'];
            $resident_room->is_dormitory = true;
            $resident_room->created = date('Y-m-d h:i:s');
            $this->Reservations->Rooms->save($resident_room);
        }

        $data['start_time'] = str_replace('T', ' ', $data['start_time']);
        $data['end_time'] = str_replace('T', ' ', $data['end_time']);

        $reservation = $this->Reservations->newEntity();
        $reservation = $this->Reservations->patchEntity($reservation, $data);
        $reservation->resident_room_id = $resident_room->id;
        $reservation->created = date('Y-m-d h:i:s');

        if ($this->Reservations->save($reservation)) {
            $saved = true;
            $id = $reservation->id;
        } else {
            $saved = false;
            $error = 'Fehlgeschlagen. Es wurde nicht gespeichert.';
        }


        $this->set(compact('saved', 'id', 'error'));
        $this->set('_serialize', ['saved', 'id', 'error']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Reservation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $data = (array) $this->request->input('json_decode');
        $reservation = $this->Reservations->get($id);
        $resident_room = $this->Reservations->Rooms->find('all')->where([
                    'name' => $data['resident_room'],
                    'is_dormitory' => true])->first();
        if (!$resident_room) {
            $saved = false;
            $error = 'Fehlgeschlagen. Dieses Zimmer ist noch nicht im System registriert.';
        } else {
            $reservation->resident_room_id = $resident_room->id;
            $reservation->description = $data['description'];

            if ($this->Reservations->save($reservation)) {
                $saved = true;
                $id = $reservation->id;
            } else {
                $saved = false;
                $error = 'Fehlgeschlagen. Es wurde nicht gespeichert.';
            }
        }

        $this->set(compact('saved', 'id', 'error'));
        $this->set('_serialize', ['saved', 'id', 'error']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Reservation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $reservation = $this->Reservations->get($id);
        if ($this->Reservations->delete($reservation)) {
            $deleted = true;
        } else {
            $deleted = false;
        }

        $this->set(compact('deleted'));
        $this->set('_serialize', ['deleted']);
    }

    public function agenda() {
        $rooms = $this->Reservations->Rooms->find('list')
                ->where(['is_schedulable' => true, 'active' => true])
                ->orderAsc('name');

        $this->set(compact('rooms'));
    }

}
