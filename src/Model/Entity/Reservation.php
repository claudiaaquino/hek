<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Reservation Entity
 *
 * @property int $id
 * @property int $room_id
 * @property int $resource_id
 * @property int $resident_room_id
 * @property string $description
 * @property \Cake\I18n\FrozenTime $start_time
 * @property \Cake\I18n\FrozenTime $end_time
 * @property \Cake\I18n\FrozenDate $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Room $room
 * @property \App\Model\Entity\Resource $resource
 * @property \App\Model\Entity\Resident $resident
 */
class Reservation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'room_id' => true,
        'resource_id' => true,
        'resident_room_id' => true,
        'start_time' => true,
        'end_time' => true,
        'description' => true,
        'created' => true,
        'modified' => true,
        'room' => true,
        'resource' => true,
        'resident' => true
    ];
}
