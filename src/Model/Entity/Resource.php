<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Resource Entity
 *
 * @property int $id
 * @property int $room_id
 * @property string $name
 * @property string $description
 * @property bool $is_schedulable
 * @property bool $active
 * @property string $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Room $room
 * @property \App\Model\Entity\Reservation[] $reservations
 */
class Resource extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'room_id' => true,
        'name' => true,
        'description' => true,
        'is_schedulable' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'room' => true,
        'reservations' => true
    ];
}
