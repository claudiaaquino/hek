<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Room Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property bool $is_dormitory
 * @property bool $is_schedulable
 * @property bool $has_resources
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Reservation[] $reservations
 * @property \App\Model\Entity\Resident[] $residents
 * @property \App\Model\Entity\Resource[] $resources
 */
class Room extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'is_dormitory' => true,
        'is_schedulable' => true,
        'has_resources' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'reservations' => true,
        'residents' => true,
        'resources' => true
    ];
}
