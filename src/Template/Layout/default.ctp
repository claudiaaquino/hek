<!DOCTYPE html>
<html lang="pt">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            HEK
        </title>        
        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('custom.min.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>    
        <?= $this->Html->script('/vendors/jquery/dist/jquery.min.js', array('inline' => false)) //necessário para carregas os outros js dentro dos content's ?>
        <?= $this->fetch('script') ?>
        <style type="text/css">
            body a:hover {
                color: #194f8c;
            }
        </style>
    </head>
    <?= $this->fetch('content') ?>

    <?= $this->Html->script('/vendors/bootstrap/dist/js/bootstrap.min.js') ?>
    <?= $this->Html->script('custom.min.js') ?>
    <?= $this->fetch('script') ?>
</html>
