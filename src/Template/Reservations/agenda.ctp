<?= $this->Html->css('/vendors/fullcalendar/dist/fullcalendar.min.css') ?>
<?= $this->Html->css('/vendors/fullcalendar/dist/fullcalendar.print.css', ['media' => 'print']) ?>
<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>

<div class="row">
    <div class="" role="main">
        <div class="col-md-4 col-sm-4 col-xs-12">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Filter</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a onclick="$('.instructions').toggle()();">
                                    Anleitung <i class="fa fa-info-circle"></i>
                                </a>
                            </li>
                            <li>
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">                   
                        <form name='formFiltros' id='formFiltros' class="form-horizontal form-label-left">
                            <?= $this->Flash->render() ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_id">Raum 
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->control('room_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $rooms, "style" => "width: 100%"]); ?>
                                </div> 
                            </div>                            
                            <div class="form-group resources">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resource_id">Resource 
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->control('resource_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div>                            
                            <div class="ln_solid"></div>
                            <!--</div>-->         
                            <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <button type="button" id='submitFiltros' class="btn btn-success">Kalender zeigen</button>                 
                                </div>
                            </div>
                        </form>
                    </div>         
                </div>

            </div>
            <div class="instructions col-md-12 col-sm-12 col-xs-12" style="display: none;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Anleitung</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </li>
                            <li>
                                <a onclick="$('.instructions').hide();">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">  
                        - 
                    </div>         
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 id="calendar-title">Kalender</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?= $this->Flash->render() ?>
                    <div id='calendar'></div>
                </div>
            </div>
        </div>

    </div>  
</div>

<!-- calendar modal -->
<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div id="testmodal" style="padding: 5px 20px;">
                    <form id="antoform" class="form-horizontal calender" role="form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Zimmer</label>
                            <div class="col-sm-9">
                                <?=
                                $this->Form->control('add_zimmer', [
                                    "class" => "form-control col-md-7 col-xs-12",
                                    'label' => false, 'placeholder' => 'e.g. 115',
                                    "data-inputmask" => "'mask': '999'",
                                    'autofocus' => 'autofocus']);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Zusätzliche Anmerkung</label>
                            <div class="col-sm-9">
                                <?=
                                $this->Form->control('add_description', ['type' => 'textarea',
                                    "class" => "form-control col-md-7 col-xs-12",
                                    'label' => false, 'height' => '55px']);
                                ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Abbrechen</button>
                <button type="button" class="btn btn-primary antosubmit">Speichern</button>
            </div>
        </div>
    </div>
</div>
<div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel2"></h4>
            </div>
            <div class="modal-body">
                <div id="testmodal2" style="padding: 5px 20px;">
                    <form id="antoform2" class="form-horizontal calender" role="form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Zimmer</label>
                            <div class="col-sm-9">
                                <?=
                                $this->Form->control('edit_zimmer', [
                                    "class" => "form-control col-md-7 col-xs-12",
                                    'label' => false, 'placeholder' => 'e.g. 115',
                                    "data-inputmask" => "'mask': '999'",
                                    'autofocus' => true]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Zusätzliche Anmerkung</label>
                            <div class="col-sm-9">
                                <?=
                                $this->Form->control('edit_description', ['type' => 'textarea',
                                    "class" => "form-control col-md-7 col-xs-12",
                                    'label' => false, 'height' => '55px']);
                                ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Abbrechen</button>
                <button type="button" class="btn btn-danger delete" data-dismiss="modal">Löschen</button>
                <button type="button" class="btn btn-primary antosubmit2">Speichern</button>
            </div>
        </div>
    </div>
</div>

<div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
<div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>
<!-- /calendar modal -->

<?= $this->Html->script('/vendors/moment/min/moment.min.js'); ?>
<?= $this->Html->script('/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>
<?= $this->Html->script('/vendors/fullcalendar/dist/fullcalendar.min.js'); ?>
<?= $this->Html->script('/vendors/fullcalendar/dist/lang/de.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>

<script>
    var started, ended, currentEvent;

    $(document).keypress(function (e) {
        if ((e.keycode == 13 || e.which == 13)) {
            e.preventDefault();
            if ($("#CalenderModalNew").hasClass('in')) {
                $(".antosubmit").click();
            } else if ($("#CalenderModalEdit").hasClass('in')) {
                $(".antosubmit2").click();
            } else {
                $("#submitFiltros").click();
            }
        }
    });

    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaWeek,agendaDay'
        },
        defaultView: 'agendaWeek',
        locale: 'de',
        allDaySlot: false,
        selectable: true,
        selectHelper: true,
        editable: true,
        eventDurationEditable: false,
        slotDuration: '01:00:00',
        displayEventTime: false,
        nowIndicator: true,
        select: function (start, end, allDay) {
            $('#fc_create').click();
            started = start;
            ended = end;
        },
        eventClick: function (calEvent, jsEvent, view) {
            $('#fc_edit').click();
            $("#edit-zimmer").val(calEvent.title);
            $("#edit-description").val(calEvent.description);
            currentEvent = calEvent;
        }
    });

    $(document).ready(function () {
        $("select").select2();
        $(":input").inputmask();
        $('.resources').hide();
        $('#room-id').change(function () {
            reloadResources($(this).val());
        });
        reloadResources($(this).val());
        $("#submitFiltros").click(function () {
            var title = 'Kalender von ' + $('#room-id option:selected').text();
            if ($('#resource-id').val()) {
                title += ' - ' + $('#resource-id option:selected').text();
            }
            $('#calendar-title').html(title);
            reloadCalendar();
        });

        $("#CalenderModalNew").on('shown.bs.modal', function () {
            $('#add-zimmer').focus();
            var title = 'Neuer Termin für ' + $('#room-id option:selected').text();
            if ($('#resource-id').val()) {
                title += ' - ' + $('#resource-id option:selected').text();
            }
            $('#myModalLabel').html(title);
        });

        $("#CalenderModalEdit").on('shown.bs.modal', function () {
            $('#edit-zimmer').focus();
            var title = 'Termin ändern für ' + $('#room-id option:selected').text();
            if ($('#resource-id').val()) {
                title += ' - ' + $('#resource-id option:selected').text();
            }
            $('#myModalLabel2').html(title);
        });

        $(".antosubmit").on("click", function () {
            if ($("#add-zimmer").val()) {
                add(started, ended);
            }
            return false;
        });

        $(".antosubmit2").on("click", function () {
            if ($("#edit-zimmer").val()) {
                edit();
            }
            return false;
        });

        $(".delete").on("click", function () {
            if (confirm("Bist du dir sicher?")) {
                remove();
            }
            return false;
        });
    });

    function reloadResources(room_id) {
        $.ajax({
            url: ' <?= $this->request->getAttribute('base') ?> resources/ajax/' + room_id,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data.has_resources) {
                    var html = '';
                    $.each(data.list, function (i, item) {
                        html = html + '<option value="' + i + '">' + item + '</option>';
                    });
                    $('#resource-id').html(html).trigger('change');
                    $('.resources').show();
                } else {
                    $('.resources').hide();
                    $('#resource-id').val('');
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }

    function add(start, end) {
        $.ajax({
            url: ' <?= $this->request->getAttribute('base') ?> reservations/add',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({
                room_id: $('#room-id').val(),
                resource_id: $('#resource-id').val(),
                resident_room: $('#add-zimmer').val(),
                description: $('#add-description').val(),
                start_time: start,
                end_time: end
            }),
            success: function (data) {
                if (data.saved) {
                    calendar.fullCalendar('renderEvent', {
                        id: data.id,
                        title: $("#add-zimmer").val(),
                        description: $("#add-description").val(),
                        start: started,
                        end: ended
                    }, true);

                    $('#add-zimmer').val('');
                    $('#add-description').val('');
                    calendar.fullCalendar('unselect');
                    $('.antoclose').click();

                    new PNotify({text: 'Gespeichert.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});

                } else {
                    new PNotify({text: data.error, type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                new PNotify({text: 'Failed', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        });
    }

    function edit() {
        currentEvent.title = $("#edit-zimmer").val();
        currentEvent.description = $("#edit-description").val();

        $.ajax({
            url: ' <?= $this->request->getAttribute('base') ?> reservations/edit/' + currentEvent.id,
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify({
                resident_room: currentEvent.title,
                description: currentEvent.description
            }),
            success: function (data) {
                if (data.saved) {
                    calendar.fullCalendar('updateEvent', currentEvent);

                    $('#edit-zimmer').val('');
                    $('#edit-description').val('');
                    calendar.fullCalendar('unselect');
                    $('.antoclose2').click();

                    new PNotify({text: 'Gespeichert', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});

                } else {
                    new PNotify({text: data.error, type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                new PNotify({text: 'Failed', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        });
    }

    function remove() {
        $.ajax({
            url: '<?= $this->request->getAttribute('base') ?>reservations/delete/' + currentEvent.id,
            type: 'DELETE',
            dataType: 'json',
            success: function (data) {
                if (data.deleted) {
                    calendar.fullCalendar('removeEvents', currentEvent.id);

                    $('#edit-zimmer').val('');
                    $('#edit-description').val('');
                    calendar.fullCalendar('unselect');
                    $('.antoclose2').click();

                    new PNotify({text: 'Gelöscht', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});

                } else {
                    new PNotify({text: 'Fehlgeschlagen. Es wurde nicht gelöscht.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                new PNotify({text: 'Failed', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        });
    }

    function reloadCalendar() {
        var listReservations = Array();
        $.ajax({
            url: '<?= $this->request->getAttribute('base') ?>reservations/search',
            type: 'POST',
            dataType: 'json',
            data: $("#formFiltros").serialize(),
            success: function (data) {
                calendar.fullCalendar('removeEvents');
                if (data.reservations) {
                    data.reservations.forEach(function (reservation) {
                        listReservations.push({
                            id: reservation.id,
                            title: reservation.resident.name,
                            description: reservation.description,
                            start: reservation.start_time,
                            end: reservation.end_time
                        });
                    });
                    calendar.fullCalendar('addEventSource', listReservations);
                    new PNotify({text: 'Kalender wurde geladen.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Fehlgeschlagen.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                new PNotify({text: 'Fehlgeschlagen.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        });
    }
</script>